﻿#include <iostream>
#include <string>
#include <algorithm>
#include <unordered_map>

using namespace std;

string convertString(string input) {
    unordered_map<char, int> countMap;
    string output = "";

    // Count the frequency of each character in the input string
    for (char c : input) {
        if (isalpha(c)) {
            c = tolower(c);
        }
        if (countMap.count(c) == 0) {
            countMap[c] = 1;
        }
        else {
            countMap[c]++;
        }
    }

    // Convert each character in the input string to ( or ) based on its frequency
    for (char c : input) {
        if (isalpha(c)) {
            c = tolower(c);
        }
        if (countMap[c] == 1) {
            output += "(";
        }
        else {
            output += ")";
        }
    }

    return output;
}

int main() {
    string input1 = "din";
    string input2 = "recede";
    string input3 = "Success";
    string input4 = "(( @";

    cout << input1 << " => " << convertString(input1) << endl;
    cout << input2 << " => " << convertString(input2) << endl;
    cout << input3 << " => " << convertString(input3) << endl;
    cout << input4 << " => " << convertString(input4) << endl;

    return 0;
}